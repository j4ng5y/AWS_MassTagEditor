# AWS Mass Tag Editor
## A tool that will add, remove, or delete tags on a mass scale on AWS (includes GovCloud)

I wrote this tool to help my company out because the AWS Tag Editor is sorely lacking.

### Prereqs
This app has very few requirements, but the three that exist are:
* Internet connection
* Python >= 3.6.5 (due to the use of f-strings that was introduced in 3.6.5)
* Pip >9.0.1
* An AWS Programmatic Access Key Pair

### Installation
1) Clone this repo locally
2) Create a python virtual environment:
  * `python3 -m venv venv`
3) Activate that environment:
  * `source venv/bin/activate`
4) Run `pip install poetry`
4) Run `poetry install -v`
5) Run `poetry run src/AWSMassTagEditor.py [ run | dryrun| modify_run | modify_dryrun ]`
  * do not actually run all the commands in the square brackets, just pick one :)
  
# Pyup Satefy Check
```
╒══════════════════════════════════════════════════════════════════════════════╕
│                                                                              │
│                               /$$$$$$            /$$                         │
│                              /$$__  $$          | $$                         │
│           /$$$$$$$  /$$$$$$ | $$  \__//$$$$$$  /$$$$$$   /$$   /$$           │
│          /$$_____/ |____  $$| $$$$   /$$__  $$|_  $$_/  | $$  | $$           │
│         |  $$$$$$   /$$$$$$$| $$_/  | $$$$$$$$  | $$    | $$  | $$           │
│          \____  $$ /$$__  $$| $$    | $$_____/  | $$ /$$| $$  | $$           │
│          /$$$$$$$/|  $$$$$$$| $$    |  $$$$$$$  |  $$$$/|  $$$$$$$           │
│         |_______/  \_______/|__/     \_______/   \___/   \____  $$           │
│                                                          /$$  | $$           │
│                                                         |  $$$$$$/           │
│  by pyup.io                                              \______/            │
│                                                                              │
╞══════════════════════════════════════════════════════════════════════════════╡
│ REPORT                                                                       │
│ checked 7 packages, using default DB                                         │
╞══════════════════════════════════════════════════════════════════════════════╡
│ No known security vulnerabilities found.                                     │
╘══════════════════════════════════════════════════════════════════════════════╛
