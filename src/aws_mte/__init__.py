#!/usr/bin/python3

"""
This script sets up either an AWS API Client or Client Paginator. The script also defines a tag class used in
submodules.
"""

import boto3
import os

# Determine how to access AWS:
AWS_ACCESS_KEY_ID = os.environ.get('AWS_ACCESS_KEY_ID') or '<your super secret key>'
AWS_SECRET_ACCESS_KEY = os.environ.get('AWS_SECRET_ACCESS_KEY') or '<your double super secret key>'
AWS_DEFAULT_REGION = os.environ.get('AWS_DEFAULT_REGION') or '<your default region>'

# Sets up AWS API Connection session:
session = boto3.Session(aws_access_key_id=AWS_ACCESS_KEY_ID,
                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                        region_name=AWS_DEFAULT_REGION)


def init_client(service):
    """This function defines an AWS API Client session.

    :param service(string): The name of the AWS API Service to access.
    :return: Returns the AWS API Client for use in submodules.
    """
    client = session.client('{}'.format(service))
    return client


def init_paginator(service, command):
    """This function defines an AWS API Client Paginator session.

    :param service(string): The name of the AWS API Service to access.
    :param command(string): The AWS API Client Paginator command API to access.
    :return: Returns the AWS API Client Paginator for use in submodules.
    """

    client = session.client('{}'.format(service))
    paginator = client.get_paginator('{}'.format(command))
    return paginator


class Tags(object):
    """
    This class defines the tags that will be added to services.
    """
    def __init__(self, tagkey, tagvalue):
        """The class __init__ function accepts the tagkey and tagvalue variables and assigns them to class objects.

        :param tagkey(sting): The Key value of the tag.
        :param tagvalue(sting): The Value value of the tag.
        """
        self.tagkey = tagkey
        self.tagvalue = tagvalue


__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.0"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", ]
__copyright__ = "Copyright 2018, Gregory Development"
