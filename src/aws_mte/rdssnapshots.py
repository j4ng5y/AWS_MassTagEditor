from aws_mte import init_paginator, init_client


def run(k, v):
    print('Initializing Connection')
    rdsclient = init_client('rds')

    print('Getting Response Object')
    rdspage = init_paginator('rds', 'describe_db_snapshots')

    response = rdspage.paginate()

    print('Adding Tag')
    try:
        for a in response:
            for b in range(0, len(a['DBSnapshots'])):
                result =rdsclient.add_tags_to_resource(
                    ResourceName = '{}'.format(a['DBSnapshots'][b]['DBSnapshotArn']),
                    Tags = [{
                        'Key': '{}'.format(k),
                        'Value': '{}'.format(v)
                        }])
                print(result)
    except:
        print('Failed')


def dryrun(k, v):
    print('Initializing Connection')
    print('Getting Response Object')
    print('Adding Tag')
    print('There is no dryrun for this, so trust me on this one :)')


__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"
