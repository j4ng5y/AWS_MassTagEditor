"""
Tags a value for the identified Tag for a all instances unless the user identifies a tag to filter on.
"""

from botocore.exceptions import ClientError
from aws_mte import init_client


ec2 = init_client('ec2')
rds = init_client('rds')
s3 = init_client('s3')


def ec2instances(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    # Get a list of instances with the appropriate fk/fv pair
    if fk is not None:
        instances = ec2.describe_instances(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        instances = ec2.describe_instaces()

    inst_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(instances['Reservations'])):
        for b in range(0, len(instances['Reservations'][a]['Instances'])):
            inst_list.append(instances['Reservations'][a]['Instances'][b]['InstanceId'])

    print('Tagging the following instances:')
    for a in inst_list:
        print(a)

    try:
        ec2.delete_tags(
            Resources=inst_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Error... {err}')

    # Create new tags with the Key value previously assigned
    try:
        ec2.create_tags(
            Resources=inst_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Error... {err}')


def ec2images(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        images = ec2.describe_images(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        images = ec2.describe_images()

    img_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(images['Images'])):
        img_list.append(images['Images'][a]['ImageId'])

    print('Tagging the following images:')
    for a in img_list:
        print(a)

    try:
        ec2.delete_tags(
            Resources=img_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Error... {err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            Resources=img_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Error...{err}')


def ec2snapshots(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        snapshots = ec2.describe_snapshots(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        snapshots = ec2.describe_snapshots()

    ss_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(snapshots['Snapshots'])):
        ss_list.append(snapshots['Snapshots'][a]['SnapshotId'])

    print('Tagging the following snapshots:')
    for a in ss_list:
        print(a)

    try:
        ec2.delete_tags(
            Resources=ss_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Error...{err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            Resources=ss_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Error...{err}')


def ec2volumes(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        volumes = ec2.describe_volumes(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ],
        )
    else:
        volumes = ec2.describe_volumes()

    vol_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(volumes['Volumes'])):
        vol_list.append(volumes['Volumes'][a]['VolumeId'])

    print('Tagging the following volumes:')
    for a in vol_list:
        print(a)

    try:
        ec2.delete_tags(
            Resources=vol_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Error...{err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            Resources=vol_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Error...{err}')


def rdsinstances(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    dbinstances = rds.describe_db_instances()
    for a in range(0, len(dbinstances['DBInstances'])):
        base = dbinstances['DBInstances'][a]

        ARN = base['DBInstanceArn']

        try:
            tags = rds.list_tags_for_resource(ResourceName=ARN)
            for b in range(0, len(tags['TagList'])):
                if fk is not None:
                    if tags['TagList'][b]['Key'] == f'{fk}':
                        if tags['TagList'][b]['Value'] == f'{fv}':
                            print(f'Tagging the DBInstance {ARN}')
                            rds.remove_tags_from_resource(
                                ResourceName=ARN,
                                TagKeys=[
                                    f'{k}'
                                ]
                            )
                            rds.add_tags_to_resource(
                                ResourceName=ARN,
                                Tags=[
                                    {
                                        'Key': f'{k}',
                                        'Value': f'{v}'
                                    }
                                ]
                            )
                else:
                    print(f'Tagging the DBInstance {ARN}')
                    rds.remove_tags_from_resource(
                        ResourceName=ARN,
                        TagKeys=[
                            f'{k}'
                        ]
                    )
                    rds.add_tags_to_resource(
                        ResourceName=ARN,
                        Tags=[
                            {
                                'Key': f'{k}',
                                'Value': f'{v}'
                            }
                        ]
                    )
        except KeyError:
            pass


def rdssnapshots(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    dbinstances = rds.describe_db_instances()
    dbsnapshots = rds.describe_db_snapshots()

    inst = []
    snap = []

    for a in range(0, len(dbsnapshots['DBSnapshots'])):
        base = dbsnapshots['DBSnapshots'][a]

        ARN = base['DBSnapshotArn']

        ARNElements = ARN.split(':')

        ARNResource = ARNElements[6]

        snap.append(
            {
                'ARN': f'{ARN}',
                'Resource': f'{ARNResource}'
            }
        )

    for a in range(0, len(dbinstances['DBInstances'])):
        base = dbinstances['DBInstances'][a]

        instARN = base['DBInstanceArn']

        instARNElements = instARN.split(':')

        instARNResource = instARNElements[6]

        inst.append(
            {
                'ARN': f'{instARN}',
                'Resource': f'{instARNResource}'
            }
        )

    for a in range(0, len(inst)):
        for b in range(0, len(snap)):
            if inst[a]['Resource'] in snap[b]['Resource']:
                instARN = inst[a]['ARN']
                snapARN = snap[b]['ARN']
                try:
                    tags = rds.list_tags_for_resource(ResourceName=f'{instARN}')
                    for b in range(0, len(tags['TagList'])):
                        if fk is not None:
                            if tags['TagList'][b]['Key'] == f'{fk}':
                                if tags['TagList'][b]['Value'] == f'{fv}':
                                    print(f'Tagging the DBSnapshot {snapARN}')
                                    rds.remove_tags_from_resource(
                                        ResourceName=f'{snapARN}',
                                        TagKeys=[
                                            f'{k}'
                                        ]
                                    )
                                    rds.add_tags_to_resource(
                                        ResourceName=f'{snapARN}',
                                        Tags=[
                                            {
                                                'Key': f'{k}',
                                                'Value': f'{v}'
                                            }
                                        ]
                                    )
                        else:
                            print(f'Tagging the DBSnapshot {snapARN}')
                            rds.remove_tags_from_resource(
                                ResourceName=f'{snapARN}',
                                TagKeys=[
                                    f'{k}'
                                ]
                            )
                            rds.add_tags_to_resource(
                                ResourceName=f'{snapARN}',
                                Tags=[
                                    {
                                        'Key': f'{k}',
                                        'Value': f'{v}'
                                    }
                                ]
                            )
                except KeyError:
                    pass


def ec2instances_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    # Get a list of instances with the appropriate Tag Key
    if fk is not None:
        instances = ec2.describe_instances(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        instances = ec2.describe_instances()

    inst_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(instances['Reservations'])):
        for b in range(0, len(instances['Reservations'][a]['Instances'])):
            inst_list.append(instances['Reservations'][a]['Instances'][b]['InstanceId'])

    print('Tagging the following instances:')
    for a in inst_list:
        print(a)

    try:
        ec2.delete_tags(
            DryRun=True,
            Resources=inst_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Success... {err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            DryRun=True,
            Resources=inst_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Success... {err}')


def ec2images_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        images = ec2.describe_images(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        images = ec2.describe_images()

    img_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(images['Images'])):
        img_list.append(images['Images'][a]['ImageId'])

    print('Tagging the following images:')
    for a in img_list:
        print(a)

    try:
        ec2.delete_tags(
            DryRun=True,
            Resources=img_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Success... {err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            DryRun=True,
            Resources=img_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Success...{err}')


def ec2snapshots_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        snapshots = ec2.describe_snapshots(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ]
        )
    else:
        snapshots = ec2.describe_snapshots()

    ss_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(snapshots['Snapshots'])):
        ss_list.append(snapshots['Snapshots'][a]['SnapshotId'])

    print('Tagging the following snapshots:')
    for a in ss_list:
        print(a)

    try:
        ec2.delete_tags(
            DryRun=True,
            Resources=ss_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Success...{err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            DryRun=True,
            Resources=ss_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Success...{err}')


def ec2volumes_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    if fk is not None:
        volumes = ec2.describe_volumes(
            Filters=[
                {
                    'Name': f'tag:{fk}',
                    'Values': [
                        f'{fv}'
                    ]
                }
            ],
        )
    else:
        volumes = ec2.describe_volumes()

    vol_list = []

    # Delete the pre-existing Tag Key
    for a in range(0, len(volumes['Volumes'])):
        vol_list.append(volumes['Volumes'][a]['VolumeId'])

    print('Tagging the following volumes:')
    for a in vol_list:
        print(a)

    try:
        ec2.delete_tags(
            DryRun=True,
            Resources=vol_list,
            Tags=[
                {
                    'Key': f'{k}',
                }
            ]
        )
    except ClientError as err:
        print(f'Success...{err}')

    # Create new tags with the value previously assigned
    try:
        ec2.create_tags(
            DryRun=True,
            Resources=vol_list,
            Tags=[
                {
                    'Key': f'{k}',
                    'Value': f'{v}'
                }
            ]
        )
    except ClientError as err:
        print(f'Success...{err}')


def rdsinstances_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    dbinstances = rds.describe_db_instances()
    for a in range(0, len(dbinstances['DBInstances'])):
        base = dbinstances['DBInstances'][a]

        ARN = base['DBInstanceArn']

        try:
            tags = rds.list_tags_for_resource(ResourceName=ARN)
            for b in range(0, len(tags['TagList'])):
                if fk is not None:
                    if tags['TagList'][b]['Key'] == f'{fk}':
                        if tags['TagList'][b]['Value'] == f'{fv}':
                            print(f'Tagging the DBInstance {ARN}')
                            print('No Dry Run, so trust me :)')
                else:
                    print(f'Tagging the DBInstance {ARN} with Key:{k}, Value:{v}')
                    print('No Dry Run, so trust me :)')
        except KeyError:
            pass


def rdssnapshots_dryrun(k, v, fk=None, fv=None):
    """Looks for a particular key and tags it with a particular value.

    :param k(string): The tag Key to modify
    :param v(string): The tag Value to modify
    :param fk(string), Default = None: The tag Key to filter on
    :param fv(string), Default = None: The tag Value to filter on
    :return: Returns notification about progress.
    """
    dbinstances = rds.describe_db_instances()
    dbsnapshots = rds.describe_db_snapshots()

    inst = []
    snap = []

    for a in range(0, len(dbsnapshots['DBSnapshots'])):
        base = dbsnapshots['DBSnapshots'][a]

        ARN = base['DBSnapshotArn']

        ARNElements = ARN.split(':')

        ARNResource = ARNElements[6]

        snap.append(
            {
                'ARN': f'{ARN}',
                'Resource': f'{ARNResource}'
            }
        )

    for a in range(0, len(dbinstances['DBInstances'])):
        base = dbinstances['DBInstances'][a]

        instARN = base['DBInstanceArn']

        instARNElements = instARN.split(':')

        instARNResource = instARNElements[6]

        inst.append(
            {
                'ARN': f'{instARN}',
                'Resource': f'{instARNResource}'
            }
        )

    for a in range(0, len(inst)):
        for b in range(0, len(snap)):
            if inst[a]['Resource'] in snap[b]['Resource']:
                instARN = inst[a]['ARN']
                snapARN = snap[b]['ARN']
                try:
                    tags = rds.list_tags_for_resource(ResourceName=f"{instARN}")
                    for b in range(0, len(tags['TagList'])):
                        if fk is not None:
                            if tags['TagList'][b]['Key'] == f'{fk}':
                                if tags['TagList'][b]['Value'] == f'{fv}':
                                    print(f"Tagging the DBSnapshot {snapARN}")
                                    print('No Dry Run, so trust me :)')
                        else:
                            print(f"Tagging the DBSnapshot {snapARN} with Key:{k}, Value:{v}")
                            print('No Dry Run, so trust me :)')
                except KeyError:
                    pass


__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"
