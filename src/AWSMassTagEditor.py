#!/usr/bin/python3

"""
Tags all resources with a key/value pair that the user determines.
"""

import sys
import re
from aws_mte import ec2instances, \
    ec2images, \
    ec2snapshots, \
    ec2subnets, \
    ec2volumes, \
    rdsinstances, \
    rdssnapshots, \
    s3buckets, \
    retag, \
    Tags, \
    AWS_DEFAULT_REGION


def main():
    """Tags all of the images or tests a run of the tagging src.

    Keywork Arguments:
    :param run(string): If the user provides the run argument at runtime, the script runs normally.
    :param dryrun(string): If the user provides the dryrun argument at runtime, the script does a test run.
    :param modify_run(string): If the user provides the modify_run argument at runtime, the script modifies tags.
    :param modify_dryrun(string): If the user provides the modify_dryrun argument at runtime, the script does a test.
    :param error: If the user does not provide an argument, or provides too many arguments, or provides the wrong
    argument,the script will return the USEAGE string.

    :return: Notifies the user of the progress throughout the scripts processes.
    """

    # This is the usage string that is printed upon runtime argument error
    usage = """ USAGE: python3 AWSMassTagEditor.py [run|dryrun|modify_run|modify_dryrun] """

    # This section used regular expressions to accept any capitalization variation of run or dryrun
    run = re.compile('run', re.IGNORECASE)
    dryrun = re.compile('dryrun', re.IGNORECASE)
    modify_run = re.compile('modify_run', re.IGNORECASE)
    modify_dryrun = re.compile('modify_dryrun', re.IGNORECASE)

    # Logic to process runtime arguments
    if len(sys.argv) == 1:
        print(""" Not Enough Arguments """)
        print(usage)
    elif len(sys.argv) > 2:
        print(""" Too Many Arguments """)
        print(usage)
    elif run.match(sys.argv[1]):

        # Ask for tag info
        tagkey = input('What tag would you like to add?: ')
        tagvalue = input('What value would you like to associate with that tag?: ')
        tags = Tags(tagkey, tagvalue)

        # Getting the information for the EC2 Instances, and then tagging all EC2 Instances
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Instances')
        ec2instances.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Images and then tagging all EC2 Images
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Images')
        ec2images.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Snapshots and then tagging all EC2 Snapshots
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Snapshots')
        ec2snapshots.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Volumes and then tagging all EC2 Volumes
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Volumes')
        ec2volumes.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 VPC Subnets and then tagging all EC2 VPC Subnets
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 VPC Subnets')
        ec2subnets.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the RDS Instances and then tagging all RDS Instances
        print(f'Tagging your {AWS_DEFAULT_REGION} RDS Instances')
        rdsinstances.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the RDS Snapshots and then tagging all RDS Snapshots
        print(f'Tagging your {AWS_DEFAULT_REGION} RDS Snapshots')
        rdssnapshots.run(tags.tagkey, tags.tagvalue)

        # Getting the information for the S3 Buckets and then tagging all S3 Buckets
        print(f'Tagging your {AWS_DEFAULT_REGION} S3 Buckets')
        s3buckets.run(tags.tagkey, tags.tagvalue)

    elif dryrun.match(sys.argv[1]):

        # Ask for tag info
        tagkey = input('What tag would you like to add?: ')
        tagvalue = input('What value would you like to associate with that tag?: ')
        tags = Tags(tagkey, tagvalue)

        # Getting the information for the EC2 Instances, and then tagging all EC2 Instances
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Instances')
        ec2instances.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Images and then tagging all EC2 Images
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Images')
        ec2images.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Snapshots and then tagging all EC2 Snapshots
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Snapshots')
        ec2snapshots.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 Volumes and then tagging all EC2 Volumes
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 Volumes')
        ec2volumes.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the EC2 VPC Subnets and then tagging all EC2 VPC Subnets
        print(f'Tagging your {AWS_DEFAULT_REGION} EC2 VPC Subnets')
        ec2subnets.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the RDS Instances and then tagging all RDS Instances
        print(f'Tagging your {AWS_DEFAULT_REGION} RDS Instances')
        rdsinstances.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the RDS Snapshots and then tagging all RDS Snapshots
        print(f'Tagging your {AWS_DEFAULT_REGION} RDS Snapshots')
        rdssnapshots.dryrun(tags.tagkey, tags.tagvalue)

        # Getting the information for the S3 Buckets and then tagging all S3 Buckets
        print(f'Tagging your {AWS_DEFAULT_REGION} S3 Buckets')
        s3buckets.dryrun(tags.tagkey, tags.tagvalue)

    elif modify_run.match(sys.argv[1]):

        # Ask for Key/Value Pair
        k = input('Please enter the Key you wish to modify: ')
        v = input('Please enter the modified Value of the Key: ')

        question = input('Do you wish to change the values based on a key filter? (y/N): ')
        y = re.compile('y', re.IGNORECASE)

        if y.match(question):
            fk = input('Please enter the Key you wish to filter on: ')
            fv = input('Please enter the Value of the Key you wish to filter on: ')
            retag.ec2instances(k, v, fk, fv)
            retag.ec2images(k, v, fk, fv)
            retag.ec2snapshots(k, v, fk, fv)
            retag.ec2volumes(k, v, fk, fv)
            retag.rdsinstances(k, v, fk, fv)
            retag.rdssnapshots(k, v, fk, fv)

        else:
            retag.ec2instances(k, v)
            retag.ec2images(k, v)
            retag.ec2snapshots(k, v)
            retag.ec2volumes(k, v)
            retag.rdsinstances(k, v)
            retag.rdssnapshots(k, v)

    elif modify_dryrun.match(sys.argv[1]):

        # Ask for Key/Value Pair
        k = input('Please enter the Key you wish to modify: ')
        v = input('Please enter the modified Value of the Key: ')

        question = input('Do you wish to change the values based on a key filter? (y/N): ')
        y = re.compile('y', re.IGNORECASE)

        if y.match(question):
            fk = input('Please enter the Key you wish to filter on: ')
            fv = input('Please enter the Value of the Key you wish to filter on: ')
            retag.ec2instances_dryrun(k, v, fk, fv)
            retag.ec2images_dryrun(k, v, fk, fv)
            retag.ec2snapshots_dryrun(k, v, fk, fv)
            retag.ec2volumes_dryrun(k, v, fk, fv)
            retag.rdsinstances_dryrun(k, v, fk, fv)
            retag.rdssnapshots_dryrun(k, v, fk, fv)

        else:
            retag.ec2instances_dryrun(k, v)
            retag.ec2images_dryrun(k, v)
            retag.ec2snapshots_dryrun(k, v)
            retag.ec2volumes_dryrun(k, v)
            retag.rdsinstances_dryrun(k, v)
            retag.rdssnapshots_dryrun(k, v)

    else:
        print(""" Unrecognized Argument """)
        print(usage)


if __name__ == "__main__":
    main()

__author__ = "Jordan Gregory"
__maintainer__ = "Jordan Gregory"
__email__ = "gregory.jordan.m@gmail.com"
__version__ = "1.0.1"
__license__ = "MIT"
__date__ = "2018 May 26"
__status__ = "Production"
__credits__ = [ "Jordan Gregory", "Michael Pigott", ]
__copyright__ = "Copyright 2018, Gregory Development"
